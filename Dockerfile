FROM selenium/standalone-chrome:95.0

USER root

RUN apt-get update -qqy \
    && apt-get install -qqy nodejs npm

CMD ["/bin/bash"]